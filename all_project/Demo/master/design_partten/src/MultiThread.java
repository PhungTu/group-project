public class MultiThread {
    public static void main(String[] args) {
        Thread thread = new Thread(new ThreadFoo());
        Thread thread1 = new Thread(new ThreadBar());

        thread.start();
        thread1.start();
    }

    static class ThreadFoo implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("Foo");
            System.out.println(singleton.getValue());
        }
    }

    static class ThreadBar implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("Bar");
            System.out.println(singleton.getValue());
        }
    }
}
