package com.example.controller;

import com.example.bo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.service.OrdersService;

@RestController
@RequestMapping("/v2/product/")
public class ProductController {

    @Autowired
    private OrdersService ordersService;

    @GetMapping("/message")
    public String getMessage(){
        Order order = this.ordersService.getOrderById(1);
        return "get message success";
    }
    @GetMapping("/checked")
    public String checked(){
        return "get message checked";
    }
}
