package com.example.service;

import com.example.bo.Order;
import com.example.dao.OrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdersServiceImpl implements OrdersService{
    @Autowired
    private OrderDao orderDao;

    @Override
    public Order getOrderById(Integer id) {
        return orderDao.getOrderById(id);
    }
}
