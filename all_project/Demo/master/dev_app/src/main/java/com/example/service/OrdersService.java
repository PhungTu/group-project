package com.example.service;

import com.example.bo.Order;

public interface OrdersService {
    Order getOrderById(Integer id);
}
